//
//  ModelsMapperService.swift
//  SetixelaMVCKit
//
//  Created by Aleksandr Solovyev on 30.12.2017.
//  Copyright © 2017 Gaozhiyuan. All rights reserved.
//

import Foundation
import UIKit

public class ModelsMapperService {
   class func map(models: inout [CellTypable], forCellType: EnumCells) {
      models.forEach { $0.cellType = forCellType }
   }
}
