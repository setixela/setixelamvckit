//
//  BaseCollectionCell.swift
//  Invercity
//
//  Created by AlexSolo on 02.05.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import SetixelaUtils

public protocol BaseCollectionCellProtocol: BaseCellProtocol {
   static func kSize() -> CGSize
   var cellSize: CGSize { get }
}

open class BaseCollectionCell: UICollectionViewCell, BaseCollectionCellProtocol, CellBindable, Configurable {
   public var touchCoord: CGPoint = CGPoint.zero
   
   public var model: Contentable?
   
   public weak var actionDelegate: ActionDelegate?
   
   public var cellHeight: CGFloat {
      return type(of: self).kHeight()
   }
   
   public var indexPath: IndexPath?
   
   public var actionClosure: (() -> Void)?
   
   open class func kSize() -> CGSize {
      return CGSize(width: 50, height: 50)
   }
   
   open class func kHeight() -> CGFloat {
      return 50
   }
   
   open func bind(_ model: Contentable) -> Self {
      return self
   }
   
   public var cellSize: CGSize {
      return type(of: self).kSize()
   }
   
   open func configure() {
      print("Override configure")
   }
   
   public override init(frame: CGRect) {
      super.init(frame: frame)
      
      self.configure()
   }
   
   public required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      self.configure()
   }
}
