//
//  CollectionCellTypable.swift
//  Invercity
//
//  Created by AlexSolo on 02.05.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import SetixelaUtils

public protocol CollectionCellTypable: class {
   var cellType: EnumCollectionCells! { get set }
}

extension CollectionCellTypable where Self: JSONModelProtocol {
   public init(json: JSON?, cellType: EnumCollectionCells) {
      self.init(json: json)

      self.cellType = cellType
   }
}
