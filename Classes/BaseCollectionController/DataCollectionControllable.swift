//
//  GDPhotoCollectionProtocol.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 29.09.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation
import SetixelaUtils
/////////////////////////////////////////////////////////////////

// MARK: -------------------------------------- data controllable

public protocol DataControllableCC: class {
   var models: [Contentable] { get set }

   var dataView: DataViewable { get }
   var cellPrototype: BaseCollectionCellProtocol { get set }
   var cellReuseIdentifier: String { get set }
   var cellBundleIdentifier: String? { get }

   func reloadData()
   func reloadDataAnimated(_ animated: Bool)
   func updateCell(indexPath: IndexPath, model: Contentable?)
}

public extension DataControllableCC where Self: UIViewController {
   var cellBundleIdentifier: String? {
      return nil
   }

   public func didLoad(_ delegate: DataControllableCC) {
      setupInterface()
      setupDataSource()
   }

   public func setupInterface() {
      dataView.registerClass(type(of: cellPrototype) as AnyClass, cellIdentifier: cellReuseIdentifier, bundleIdentifier: cellBundleIdentifier)
   }

   public func setupDataSource() {
   }
}
