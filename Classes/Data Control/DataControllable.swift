//
//  GDPhotoCollectionProtocol.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 29.09.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation

/////////////////////////////////////////////////////////////////
//MARK: -------------------------------------- data viewable

public protocol DataViewable {
   func reloadData()
   func registerNib(_ nibName: String, cellIdentifier: String, bundleIdentifier: String?)
   func registerClass(_ anyClass: AnyClass, cellIdentifier: String, bundleIdentifier: String?)
}

//MARK: -------------------------------------- data controllable
public protocol DataControllable: class {
   
   var models: [Contentable] { get set }
   
   var dataView: DataViewable { get }
   var cellPrototype: BaseCellProtocol { get set }
   //  var cellProtypesA: [CellBindable] { get set }
   var cellReuseIdentifier: String { get set }
   var cellBundleIdentifier: String? { get }
   
   func reloadData()
   func reloadDataAnimated(_ animated: Bool)
   func updateCell(indexPath: IndexPath, model: Contentable?)
}

public extension DataControllable where Self: UIViewController {
   
   var cellBundleIdentifier: String? {
      return nil
   }
   
   public func didLoad(_ delegate: DataControllable) {
      setupInterface()
      setupDataSource()
   }
   
   public func setupInterface() {
      self.dataView.registerClass(type(of: cellPrototype) as AnyClass, cellIdentifier: cellReuseIdentifier, bundleIdentifier: cellBundleIdentifier)
   }
   
   public func setupDataSource() {
      
   }
}
