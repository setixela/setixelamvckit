//
//  UIViewControllerExtension.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 20.09.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation

public extension UIViewController {
	public func hideKeyboardWhenTappedAround(isCancelsTouchesInView: Bool = true) {
      let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
      tap.cancelsTouchesInView = isCancelsTouchesInView
      view.addGestureRecognizer(tap)
   }
   
   @objc func dismissKeyboard() {
      view.endEditing(true)
   }
}
