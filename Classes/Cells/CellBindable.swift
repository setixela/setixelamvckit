//
//  ModelProtocols.swift
//  Invercity
//
//  Created by AlexSolo on 03.02.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import UIKit

////////////

public protocol CellBindable: Bindable {
   static func kHeight() -> CGFloat
   func bind(_ model: Contentable) -> Self
   var actionClosure:(() -> Void)? { get set }
}

public protocol GenericBindable {
   associatedtype D: Contentable
   func genericBind(_ model: D) -> Self
}

public extension GenericBindable where Self: CellBindable {
   func genericBind(_ model: D) -> Self {
      return self.bind(model)
   }
}
