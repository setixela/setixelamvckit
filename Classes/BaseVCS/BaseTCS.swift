//
//  BaseTCS.swift
//  Invercity
//
//  Created by AlexSolo on 24.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import SetixelaUtils

open class BaseTCS<C: EnumCells>: BaseTCD<C>, Searchable {
   /// searchable
   public var searchController: SearchController!
   public var filteredModels: [Contentable] = [] // searchable
   
   open var searchBarHeight: CGFloat { return 48 }
   //MARK: -------------------------------------- methods controllable
   
   override open func viewDidLoad() {
      super.viewDidLoad()
      
      self.setupSearchController()
      
      view.addSubview(searchController.customSearchBar)
      searchController.customSearchBar.height = searchBarHeight
      tableView.scaleTopToY(y: searchBarHeight)
   }
   
   open func setupSearchController() {
      
      searchController = ({
         let controller = SearchController(
            searchResultsController: nil,
            searchBarFrame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 50),
            searchBarFont: UIFont.systemFont(ofSize: 14),
            searchBarTextColor: UIColor.gray,
            searchBarTintColor: UIColor.lightGray)
         
         controller.customSearchBar.placeholder = NSLocalizedString("Search here", comment: "search bar")
         // controller.searchResultsUpdater = self
         controller.dimsBackgroundDuringPresentation = false
         controller.customSearchBar.sizeToFit()
         controller.customSearchBar.showsCancelButton = false
         
         return controller
      })()
      
      searchController.customDelegate = self
   }
   
   override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if isSearchActive {
         return filteredModels.count
      } else {
         return models.count
      }
   }
   
   override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      if isSearchActive {
         let model = filteredModels[indexPath.row]
         return heightFor(model: model, indexPath: indexPath)
      } else {
         let model = models[indexPath.row]
         return heightFor(model: model, indexPath: indexPath)
      }
   }
   
   override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if isSearchActive {
         let model = filteredModels[indexPath.row]
         return cellFor(model: model, indexPath: indexPath)
      } else {
         let model = models[indexPath.row]
         
         if let vc = self as? Paginable {
            
            vc.checkAndLoadNextPage(models: models, indexPath: indexPath)
         }
         
         return cellFor(model: model, indexPath: indexPath)
      }
   }
   
   //MARK: ----------- search delegate
   open func didChangeSearchText(_ searchText: String) {
      /// override this
   }
}
