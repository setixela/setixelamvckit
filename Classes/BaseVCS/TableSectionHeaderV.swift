//
//  TableSectionHeaderV.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 01.04.17.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import SetixelaUtils
import UIKit

fileprivate let kDefaultHeaderInsets = UIEdgeInsets(top: 4, left: kGrid8 * 2, bottom: 0, right: kGrid8 * 2)

public protocol TableSectionHeaderDelegate: class {
   func register(header: TableSectionHeaderProtocol)
}

public protocol TableSectionHeaderProtocol {
   var backColor: UIColor { get set }
   var fontColor: UIColor { get set }
   
   var headerHeight: CGFloat { get set }
   
   init(text: String, height: CGFloat, backColor: UIColor, fontSize: CGFloat, fontColor: UIColor)
}

public class TableSectionHeaderView: UIView, TableSectionHeaderProtocol {
   public var headerHeight: CGFloat
   public var backColor: UIColor
   public var fontColor: UIColor
   public var fontSize: CGFloat
   
   public required init(text: String, height: CGFloat, backColor: UIColor, fontSize: CGFloat = 14, fontColor: UIColor) {
      self.backColor = backColor
      self.fontColor = fontColor
      self.fontSize = fontSize
      headerHeight = height
      
      super.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: height))
      
      backgroundColor = backColor
      headerLabel = UILabel(frame: bounds)
      headerLabel.textColor = fontColor
      headerLabel.font = UIFont.systemFont(ofSize: fontSize)
      headerLabel.text = text
      headerLabel.textAlignment = .center
      addSubview(headerLabel)
      
      setInsets(insets: kDefaultHeaderInsets)
   }
   
   var insets: UIEdgeInsets! {
      didSet {
         headerLabel.frame = CGRect(x: insets.left, y: insets.top, width: width - insets.left - insets.right, height: height - insets.top - insets.bottom)
      }
   }
   
   var textColor: UIColor!
   
   private var headerLabel: UILabel!
   
   static var kHeight: CGFloat {
      return 44
   }
   
   //   init(text: String) {
   //      super.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: type(of: self).kHeight))
   //
   //      backgroundColor = .lightGray
   //      headerLabel = UILabel(frame: bounds)
   //      headerLabel.textColor = UIColor.gray
   //      headerLabel.font = UIFont.systemFont(ofSize: 18)
   //      headerLabel.text = text
   //      headerLabel.textAlignment = .center
   //      addSubview(headerLabel)
   //
   //      setInsets(insets: kDefaultHeaderInsets)
   //   }
   //
   private func setInsets(insets: UIEdgeInsets) {
      self.insets = insets
   }
   
   public required init?(coder aDecoder: NSCoder) {
      backColor = .black
      fontColor = .white
      fontSize = 14
      headerHeight = 40
      
      super.init(coder: aDecoder)
   }
}
