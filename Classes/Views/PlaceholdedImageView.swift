//
//  PlaceholdedImageView.swift
//  Invercity
//
//  Created by AlexSolo on 27.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import SetixelaUtils

public class PlaceholdedImageView: UIView, Configurable {
   public var placeHolderImage: UIImageView!
   public var imageIV: UIImageView!
   
   public func setImageAnimated(image: UIImage, time: TimeInterval, isInstant: Bool = false, fromZero: Bool = true) {
      self.imageIV.setImageAnimated(image: image, time: time, isInstant: isInstant, fromZero: fromZero)
   }
   public var image: UIImage? {
      set {
         self.imageIV.image = newValue
      }
      get {
         return imageIV.image
      }
   }

   override public init(frame: CGRect) { super.init(frame: frame)
      self.configure()
   }
   required public init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder)
      self.configure()
   }

   public func configure() {
      
      self.clipsToBounds = true
      self.layer.cornerRadius = 3
      
      placeHolderImage = UIImageView(frame: self.bounds)
      placeHolderImage.contentMode = .scaleAspectFill
      placeHolderImage.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      placeHolderImage.image = UIImage(named: "image_stub")
      self.addSubview(placeHolderImage)
      
      imageIV = UIImageView(frame: self.bounds)
      imageIV.backgroundColor = UIColor.clear
      imageIV.contentMode = .scaleAspectFill
      imageIV.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      imageIV.image = nil// UIImage(named: "image_stub")
      self.addSubview(imageIV)
   }
}
