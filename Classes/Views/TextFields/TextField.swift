//
//  TextField.swift
//  Invercity
//
//  Created by AlexSolo on 21.02.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import UIKit

fileprivate let kCellTextOffset: CGFloat = 16

open class TextField: UITextField {

  // let label: UILabel!
   
   /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
    // Drawing code
    }
    */
   
   public var padding = UIEdgeInsets(top: 0, left: kCellTextOffset, bottom: 0, right: kCellTextOffset)
   
   override open func textRect(forBounds bounds: CGRect) -> CGRect {
      return bounds.inset(by: padding)
   }
   
   override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
      return bounds.inset(by: padding)
   }
   
   override open func editingRect(forBounds bounds: CGRect) -> CGRect {
      return bounds.inset(by: padding)
   }
   
//   func setLabel(text: String) {
//      self.label.text = text
//   }
   ////
   
   override public init(frame: CGRect) {
      super.init(frame: frame)
      self.configure()
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      self.configure()
   }
   
   open func configure() {
      self.backgroundColor = UIColor.clear
      self.tintColor = UIColor.black
      self.textColor = UIColor.gray
      self.rightViewMode = .never
      borderStyle = .none
      textAlignment = .right
      clearButtonMode = .never
      isEnabled = true
      isSecureTextEntry = false
      font = UIFont.systemFont(ofSize: 16)
      autocorrectionType = UITextAutocorrectionType.no
      keyboardType = UIKeyboardType.default
      returnKeyType = UIReturnKeyType.done
      contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
      
      let attributes = [
         NSAttributedString.Key.foregroundColor: UIColor.black,
         NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)
      ]
      attributedPlaceholder = NSAttributedString(string: "Placeholder Text", attributes: attributes)
      
//      label.backgroundColor = UIColor.clear
//      label.font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightRegular)
//      label.textColor = UIColor.gray
//      label.text = self.placeholder
//      label.numberOfLines = 0
//      label.contentMode = .center
      self.placeholder = ""
      
     // self.addSubview(label)
   }
   
   override open func layoutSubviews() {
      
      super.layoutSubviews()
      
//      label.sizeToFit()
//      label.frame = CGRect(x: kCellTextOffset, y: 0, width: label.frame.width, height: self.height)
//      label.center.y = self.height/2.0
//      self.padding.left = label.width+kCellTextOffset*2.0
   }
   
}
