//
//  Response.swift
//  Invercity
//
//  Created by AlexSolo on 02.02.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import Alamofire

public extension Response {
   public func objectFor(dataKey: DataKey)->Any? {
      return dictionary?[dataKey.rawValue]
   }
}

public class Response: NSObject {
   public var status: Status
   //var events: [JSONModel]? = [[:]]
   public var dictionary: JSON? = [:]
   //  var events: [Event] = []
   //   var event: Event? {
   //      return events.first
   //   }
   public var items: [JSON]? {
      return dictionary?["items"] as? [JSON]
   }
   
   public init(_ response: DataResponse<Any>) {
      
      self.status = Response.serializeResponseStatus(response)
      
      if let result = response.result.value {
         let JSONdata = result as? JSON
         
         let data = JSONdata?["data"]
         
         if let dic = data as? JSON {
            self.dictionary = dic
         } else {
            if let dicArray = data as? [JSON] {
               self.dictionary = dicArray.first
            }
         }
         
         //         if let eventsArray = JSONdata?["events"] as? [JSON] {
         //          //  self.events = eventsArray
         //
         //            events = eventsArray.map({ (dic) -> Event in
         //               let event = Event(eventObject: dic)
         //               return event
         //            })
         //         }
         //
         //         events.forEach({ (event) in
         //            print("Event: ", event.json.description)
         //         })
         
         if let apiErrorCode = JSONdata?["status"] as? Int {
            print("API ERROR CODE: ||||||||||||||||||||||||||||||||||||||||||||||||")
            print("API CODE:", apiErrorCode, ", DESCRIPTION:", String(describing: ApiErrorCodes[apiErrorCode]))
            
            if apiErrorCode == 403 {
               self.status = .accessDenied
            }
            if apiErrorCode == 404 {
               self.status = .notFound
            }
         }
         
      } else {
         self.dictionary = [:]
      }
   }
   
   class func serializeResponseStatus(_ response: DataResponse<Any>) -> Status {
      
      switch response.result {
      case .success:
         break
      case .failure:
         return .serializationError
      }
      
      if let status: Int = (response.response?.statusCode) {
         
         print("NETWORK RESPONSE: **********************************************")
         print("Network response status CODE:", status)
         switch status {
         case 200:
            return .success
         case 201:
            return .success
         case 204:
            return .success
         case 401:
            return .badCredentials
         case 404:
            return .notFound
         case 422:
            return .invalidEmail
         case 500:
            return .serverError
         default:
            return .unknown
         }
      } else {
         return .unknown
      }
   }
}

public let ApiErrorCodes: [Int: String] = [
   // Base
   100: "Incorrect input data. Exception while validation input data",
   
   // Field validation errors
   110: "Field must be not empty",
   111: "Field must be a numeric",
   112: "Field must be a valid r egex",
   113: "Field must be a email",
   114: "One of the several fields must be not empty",
   115: "Field can\'t equal some value",
   116: "Field value must be between",
   117: "Field must equal some value",
   118: "Field must be a valid datetime format",
   119: "Field is too short",
   
   // File validation errors /
   130: "Error while validate file",
   131: "File max size",
   132: "File allowed types",
   133: "Image file minimal resolution",
   134: "Image file maximum resolution",
   135: "Resource for file not found",
   136: "Field not found in resource",
   137: "File slot not found in DB",
   
   150: "Related object not found",
   151: "Object already exist",
   
   170: "Error while saving image",
   180: "Object already proceed",
   
   // Period validation errors /
   200: "Operation restricted because period has not passed",
   201: "User can execute operation only one time",
   
   // Custom error codes /
   300: "Old user password does\'t equal input",
   301: "Bad username or password",
   
   404: "Invalid adress"
]
