//
//  GDSearchController.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 13.10.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import SetixelaUtils
import UIKit

public protocol SearchControllerDelegate: class {
   func didStartSearching()
   func didTapOnSearchButton()
   func didTapOnCancelButton()
   func didChangeSearchText(_ searchText: String)
}

extension SearchControllerDelegate {
   public func didStartSearching() {}
   public func didTapOnSearchButton() {}
   public func didTapOnCancelButton() {}
}

// MARK: ----------- GDSearchController

public class SearchController: UISearchController, UISearchBarDelegate {
   public var customSearchBar: SearchBar!
   public weak var customDelegate: SearchControllerDelegate!
   
   public init(searchResultsController: UIViewController!, searchBarFrame: CGRect, searchBarFont: UIFont, searchBarTextColor: UIColor, searchBarTintColor: UIColor) {
      super.init(searchResultsController: searchResultsController)
      
      configureSearchBar(frame: searchBarFrame, font: searchBarFont, textColor: searchBarTextColor, bgColor: searchBarTintColor)
   }
   
   public func configureSearchBar(frame: CGRect, font: UIFont, textColor: UIColor, bgColor: UIColor) {
      customSearchBar = SearchBar(frame: frame, font: font, textColor: textColor)
      
      customSearchBar.barTintColor = bgColor
      customSearchBar.tintColor = textColor
      customSearchBar.showsBookmarkButton = false
      customSearchBar.showsCancelButton = true
      
      customSearchBar.delegate = self
   }
   
   // MARK: ----------- searchbar delegate funcs
   
   public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
      customDelegate.didStartSearching()
   }
   
   public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
      customSearchBar.resignFirstResponder()
      customDelegate.didTapOnSearchButton()
   }
   
   public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
      customSearchBar.resignFirstResponder()
      customDelegate.didTapOnCancelButton()
   }
   
   public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
      customDelegate.didChangeSearchText(searchText)
   }
   
   /// others
   override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
      super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
   }
   
   public required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
   }
   
   ////
}
