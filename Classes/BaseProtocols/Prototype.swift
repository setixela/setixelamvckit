//
//  Prototype.swift
//  GdeDuet2
//
//  Created by AlexSolo on 02.05.17.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

public enum PrototypeMode {
   case
      standard,
      extended
}

public protocol Prototype {
   static var prototype: Self { get }
}

public protocol PrototypeModable {
   var prototypeMode: PrototypeMode { get set }
}
