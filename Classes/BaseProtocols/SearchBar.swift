//
//  GDSearchBar.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 13.10.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import UIKit
import SetixelaUtils

//MARK: ----------- GDSearchBar
public class SearchBar: UISearchBar {
   
   var prefferedFont: UIFont!
   var prefferedTextColor: UIColor!
   
   //MARK: --------(•)(•)---------
   
   init(frame: CGRect, font: UIFont, textColor: UIColor) {
      super.init(frame: frame)
      
      self.frame = frame
      prefferedFont = font
      prefferedTextColor = textColor
      
      searchBarStyle = UISearchBar.Style.default
      isTranslucent = false
      
      layer.borderWidth = 1
      layer.borderColor = UIColor.gray.cgColor
      
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   func indexOfSearchFieldInSubviews() -> Int! {
      var index: Int = 0
      let searchBarView = subviews[0]
      
      for i in 0..<searchBarView.subviews.count {
         if searchBarView.subviews[i].isKind(of: UITextField.self) {
            break
         }
         index += 1
      }
      
      return index
   }
   
   override public func draw(_ rect: CGRect) {
      
      if let index = indexOfSearchFieldInSubviews() {
         if let searchField: UITextField = (subviews[0]).subviews[index] as? UITextField {
            
            searchField.frame = CGRect(x: kGrid8, y: kGrid8, width: frame.size.width - kGrid8*2, height: frame.size.height - kGrid8*2)
            
            searchField.font = prefferedFont
            searchField.textColor = prefferedTextColor
            searchField.borderStyle = .none
            searchField.layer.cornerRadius = 3
            searchField.clipsToBounds = true
            
            searchField.backgroundColor = .white
            
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: kGrid8/2, height: searchField.frame.height))
            searchField.leftView = paddingView
         }
      }
      
      super.draw(rect)
   }
}
