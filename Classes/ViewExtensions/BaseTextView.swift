//  Created by AlexSolo on 26.12.2017.
//  Copyright © 2017 Gaozhiyuan. All rights reserved.
//

import Foundation

open class BaseTextView: UITextView, Configurable {
   
   override public init(frame: CGRect, textContainer: NSTextContainer?) {
      super.init(frame: frame, textContainer: textContainer)
      
      self.configure()
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      self.configure()
   }
   
   open func configure() {
      print("Override configure!!!!")
      fatalError("Override configure!!!!")
   }
   /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
    // Drawing code
    }
    */
   
}
