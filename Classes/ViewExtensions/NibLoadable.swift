//
//  NibLoadable.swift
//  SetixelaMVCKit
//
//  Created by Aleksandr Solovyev on 30.12.2017.
//  Copyright © 2017 Gaozhiyuan. All rights reserved.
//

import Foundation
import UIKit

public protocol NibLoadable {
   associatedtype T: UIView
   static func nibView() -> T?
}

public extension NibLoadable where Self: UIView {
   static func nibView() -> Self? {
      return Bundle(for: self).loadNibNamed(String(describing: self), owner: nil, options: nil)?.first as? Self
   }
}
