//
//  ActionsService.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 07.06.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

//
// enum MediatorTypes { /// not necessery! can use ActionService.shared.activate...
//   case
//   myProfileActionMediator
//
//   //// main
//   func build() -> AnyObject {
//      switch self {
//      case .myProfileActionMediator:
//         return MyProfileActionMediator()
//      }
//   }
// }

// enum ProfileActions: ActionProtocol {
//   case
//      pushTakePhoto,
//      didTapAllPhotos
// }

import Foundation

public protocol SetRecieverProtocol: AnyObject {
   func setReciever<T>(reciever: T)
}

public protocol Mediators {
   func build() -> AnyObject
}

public protocol ActProtocol {}

public protocol PerformerProtocol {
   func perform(action: ActProtocol)
}

public protocol ActMediatorProtocol: PerformerProtocol, SetRecieverProtocol {
   associatedtype T

   var reciever: Weak<AnyObject>? { get set } // MARK: WEAK!

   var delegate: T! { get }
}

public extension ActMediatorProtocol {
   public var delegate: T! {
      // swiftlint:disable force_cast
      return (reciever!.value as! T)
      // swiftlint:enable colon
   }
}

public extension ActMediatorProtocol {
   func setReciever<T>(reciever: T) {
      self.reciever = Weak(value: reciever as AnyObject)
      //      self.reciever.value = reciever as AnyObject
   }
}

open class BaseMediator<T>: ActMediatorProtocol {
   open func perform(action: ActProtocol) {
      print("Ovverride BaseMediator<T> actions!")
   }

   public var reciever: Weak<AnyObject>?

   public init() {
   }
}
////////
public protocol SenderProtocol {
   func sendAction(_ action: ActProtocol)
}

public extension SenderProtocol {
   public func sendAction(_ action: ActProtocol) {
      ActionService.shared.perform(action: action)
   }
}

public protocol MediatorDelegate: class {
   func activate(_ type: Mediators)
}
extension MediatorDelegate {
   public func activate(_ type: Mediators) {
      ActionService.shared.activate(type, forReciever: self)
   }
}

/////////////////

class MediatorWrap {
   var mediator: AnyObject?
   var reciever: Weak<AnyObject>

   init(mediator: AnyObject, reciever: Weak<AnyObject>) {
      self.mediator = mediator
      self.reciever = reciever
   }
}

public final class ActionService: Singleton {
   var recievers: [MediatorWrap] = []

   public func perform(action: ActProtocol) {
      cleanNilRecievers()

      print("perform: ", action)
      for reciever in recievers {
         if let mediator = reciever.mediator as? PerformerProtocol {
            print("Mediator found: ", mediator)
            mediator.perform(action: action)
         }
      }
   }

   private func addReciever(_ reciever: MediatorWrap) {
      cleanNilRecievers()

      recievers.append(reciever)
   }

   public func activate<T: MediatorDelegate>(_ mediator: AnyObject, forReciever reciever: T) {
      cleanNilRecievers()

      activate(mediator: mediator, reciever: reciever)
   }

   public func activate<T: MediatorDelegate>(_ type: Mediators, forReciever reciever: T) {
      cleanNilRecievers()

      let mediator = type.build()
      activate(mediator: mediator, reciever: reciever)
   }

   private func activate<T: MediatorDelegate>(mediator: AnyObject, reciever: T) {
      if let setRecieve = mediator as? SetRecieverProtocol {
         setRecieve.setReciever(reciever: reciever)

         let newReciever = reciever as AnyObject
         let weakReciever = Weak(value: newReciever)
         let actionReciever = MediatorWrap(mediator: mediator, reciever: weakReciever)
         addReciever(actionReciever)
      } else {
         fatalError("Activate action mediator failed")
      }
   }

   public static var shared: ActionService = ActionService()

   private func cleanNilRecievers() {
      let cleanedRecievers = recievers.filter {
         $0.reciever.value != nil
      }
      recievers = cleanedRecievers
   }
}
