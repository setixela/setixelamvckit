//
//  CGSizeExtension.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 06.06.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public extension CGSize {
   func rescaled(toMaxSize maxSize: CGFloat) -> CGSize {
      let size = self
      let width = size.width
      let height = size.height
      
      var newWidth: CGFloat = width
      var newHeight: CGFloat = height
      
      if width > height && width > maxSize {
         let diff = maxSize / width
         newHeight = height * diff
         newWidth = maxSize
      } else {
         if height > width && height > maxSize {
            let diff = maxSize / height
            newWidth = width * diff
            newHeight = maxSize
         } else {
            if width == height && width > maxSize {
               newHeight = maxSize
               newWidth = maxSize
            }
         }
      }
      
      let newSize = CGSize(width: newWidth, height: newHeight)
      
      return newSize
   }
}
