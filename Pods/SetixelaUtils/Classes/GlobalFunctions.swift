//
//  GlobalFunctions.swift
//  SetixelaUtils
//
//  Created by AlexSolo on 22.12.2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

public let screenCoef = UIScreen.main.bounds.size.width / 320.0
public let screenWidth = UIScreen.main.bounds.size.width
public let screenHeight = UIScreen.main.bounds.height

public let kGrid8: CGFloat = 8

public extension NSObject {
   var theClassName: String {
      return NSStringFromClass(type(of: self))
   }
}

///// RANDOMS//////////////////////////////////////////////////////////

@available(*, deprecated, message: "use random(_ max: Int)")
public func xelaRandom(_ max: Int) -> Int {
   return Int(arc4random_uniform(UInt32(max)))
}

@available(*, deprecated, message: "use randomBool()")
public func xelaRandomBool() -> Bool {
   return arc4random_uniform(2) == 1 ? true : false
}

@available(*, deprecated, message: "use random100()")
public func xelaRandom100() -> Int {
   return Int(arc4random_uniform(UInt32(101)))
}

@available(*, deprecated, message: "use randomFloat()")
public func xelaRandomFloat() -> CGFloat {
   return CGFloat(Float(arc4random()) / Float(UINT32_MAX))
}

@available(*, deprecated, message: "use random(_ min: CGFloat, max: CGFloat))")
public func xelaRandom(_ min: CGFloat, max: CGFloat) -> CGFloat {
   var rnd = CGFloat(CGFloat(arc4random()) / CGFloat(UINT32_MAX))
   rnd *= (max - min)
   return rnd + min
}

///////// not xela dublicates

public func random(_ max: Int) -> Int {
   return Int(arc4random_uniform(UInt32(max)))
}

public func randomBool() -> Bool {
   return arc4random_uniform(2) == 1 ? true : false
}

public func random100() -> Int {
   return Int(arc4random_uniform(UInt32(101)))
}

public func randomFloat() -> CGFloat {
   return CGFloat(Float(arc4random()) / Float(UINT32_MAX))
}

public func random(_ min: CGFloat, max: CGFloat) -> CGFloat {
   var rnd = CGFloat(CGFloat(arc4random()) / CGFloat(UINT32_MAX))
   rnd *= (max - min)
   return rnd + min
}

//////////////////////////////////

import SystemConfiguration

public func isInternetAvailable() -> Bool {
   var zeroAddress = sockaddr_in()
   zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
   zeroAddress.sin_family = sa_family_t(AF_INET)

   let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
      $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
         SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
      }
   }

   var flags = SCNetworkReachabilityFlags()
   if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
      return false
   }
   let isReachable = flags.contains(.reachable)
   let needsConnection = flags.contains(.connectionRequired)
   return (isReachable && !needsConnection)
}

//////
public enum IPhoneType {
   case
      iPhone5,
      iPhone6,
      iPhone6Plus,
      iPhoneX,
      unknown

   public static var type: IPhoneType {
      let device = UIDevice()
      if device.userInterfaceIdiom == .phone {
         switch UIScreen.main.nativeBounds.height {
         case 1136:
            return .iPhone5
         case 1334:
            return .iPhone6
         case 1920, 2208:
            return .iPhone6Plus
         case 2436:
            return .iPhoneX
         default:
            return .unknown
         }
      } else {
         return .unknown
      }
   }
}
