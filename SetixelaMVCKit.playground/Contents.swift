//: A UIKit based Playground for presenting user interface

import PlaygroundSupport
import SetixelaMVCKit
import SetixelaUtils
import UIKit

protocol RecieverProtocol: AnyObject {
   func setReciever<T>(reciever: T)
}

public class Weak<T: AnyObject> {
   weak var value: T?
   public init(value: T) {
      self.value = value
   }
}

public extension Array where Element: Weak<AnyObject> {
   mutating func reap() {
      self = filter { nil != $0.value }
   }
}

enum MediatorTypes {
   case
      myProfileActionMediator

   //// main
   func build() -> AnyObject {
      switch self {
      case .myProfileActionMediator:
         return MyProfileActionMediator()
      }
   }
}

protocol ActionProtocol {}
enum ProfileActions: ActionProtocol {
   case
      pushTakePhoto,
      didTapAllPhotos
}

protocol MediatorPerformer {
   func perform(action: ActionProtocol)
}

protocol ActionMediatorProtocol: MediatorPerformer, RecieverProtocol {
   associatedtype T

   var reciever: T? { get set } // MARK: WEAK!

}

extension ActionMediatorProtocol {
   func setReciever<T>(reciever: T) {
      self.reciever = reciever as? Self.T
   }
}

////////
protocol SenderProtocol {
   func sendAction(_ action: ActionProtocol)
}

extension SenderProtocol {
   func sendAction(_ action: ActionProtocol) {
      ActionManager.shared.perform(action: action)
   }
}

protocol MediatorDelegate: class {}

/////////////////

class ActionReciever {
   var mediator: AnyObject
   var reciever: Weak<AnyObject>

   init(mediator: AnyObject, reciever: Weak<AnyObject>) {
      self.mediator = mediator
      self.reciever = reciever
   }
}

final class ActionManager {
   var recievers: [ActionReciever] = []

   func perform(action: ActionProtocol) {
      cleanNilRecievers()

      for reciever in recievers {
         if let mediator = reciever.mediator as? MediatorPerformer {
            mediator.perform(action: action)
         }
      }
   }

   private func addReciever(_ reciever: ActionReciever) {
      cleanNilRecievers()

      recievers.append(reciever)
   }

   func activate<T: MediatorDelegate>(_ type: MediatorTypes, forReciever reciever: T) {
      cleanNilRecievers()

      let mediator = type.build()
      if let setRecieve = mediator as? RecieverProtocol {
         setRecieve.setReciever(reciever: reciever)

         let newReciever = reciever as AnyObject
         let weakReciever = Weak(value: newReciever)
         let actionReciever = ActionReciever(mediator: mediator, reciever: weakReciever)
         addReciever(actionReciever)
      } else {
         fatalError("Activate action mediator failed")
      }
   }

   static var shared: ActionManager = ActionManager()

   private func cleanNilRecievers() {
      let cleanedRecievers = recievers.filter { $0.reciever.value != nil }
      recievers = cleanedRecievers
   }
}

//////////////////////////////////////////////////////////////////

class MyProfileActionMediator<T: TestDelegate>: ActionMediatorProtocol {
   weak var reciever: T?

   func perform(action: ActionProtocol) {
      switch action {
      case ProfileActions.didTapAllPhotos:
         reciever?.openPhotos()
      case ProfileActions.pushTakePhoto:
         reciever?.pushPhoto()
      default:
         break
      }
   }
}

class TestDelegate: MediatorDelegate {
   init() {
      ActionManager.shared.activate(MediatorTypes.myProfileActionMediator, forReciever: self)
   }

   func openPhotos() {
      print("Open photos")
   }

   func pushPhoto() {
      print("Push photo")
   }
}

class TestSender: SenderProtocol {
}

var testDelegate: TestDelegate? = TestDelegate()

let sender = TestSender()
sender.sendAction(ProfileActions.didTapAllPhotos)
sender.sendAction(ProfileActions.didTapAllPhotos)

testDelegate = nil

sender.sendAction(ProfileActions.didTapAllPhotos)

print(ActionManager.shared.recievers.description)

testDelegate = TestDelegate()

sender.sendAction(ProfileActions.pushTakePhoto)
